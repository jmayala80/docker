## Virtual Machines VS Containers
### Hipervisor
 * [Hipervisor (Wiki)](https://es.wikipedia.org/wiki/Hipervisor#:~:text=Un%20hipervisor%20(en%20ingl%C3%A9s%20hypervisor,de%20paravirtualizaci%C3%B3n)%20en%20una%20misma)
 * [¿Qué es un hipervisor? (Red Hat)](https://www.redhat.com/es/topics/virtualization/what-is-a-hypervisor)

![alt text](img/TiposDeHypervisores.JPG)

### Tipos de hipervisores
Para la virtualización, se pueden usar dos tipos diferentes de hipervisores: el tipo 1 y el tipo 2.

#### Tipo 1
El hipervisor de tipo 1, también conocido como hipervisor nativo o sin sistema operativo, se ejecuta directamente en el hardware del host y gestiona los sistemas operativos guest.
Este tipo de hipervisor solemos encontrarlo en un centro de datos empresarial o en otros entornos basados en servidores.
 * [Proxmox](https://www.proxmox.com/en/)
 * [KVM](https://www.linux-kvm.org/page/Main_Page)
 * [Microsoft Hyper-V](https://azure.microsoft.com/es-mx/free/virtual-machines/search/?&ef_id=CjwKCAjwq_D7BRADEiwAVMDdHscA25U611CnQSzbg5TakiNoCWj6Uu35Qc7rvyQ5y3xcLwMg_anMLhoCKscQAvD_BwE:G:s&OCID=AID2100002_SEM_CjwKCAjwq_D7BRADEiwAVMDdHscA25U611CnQSzbg5TakiNoCWj6Uu35Qc7rvyQ5y3xcLwMg_anMLhoCKscQAvD_BwE:G:s&dclid=CLb44pqQoOwCFX0ruQYdHeYEaA)
 * [VMware vSphere](https://www.vmware.com/ar/products/vsphere.html)

#### Tipo 2
El hipervisor de tipo 2 también se conoce como hipervisor alojado, y se ejecuta en un sistema operativo convencional como una capa de software o una aplicación.
El hipervisor de tipo 2 es mejor para los usuarios individuales que buscan ejecutar varios sistemas operativos en una computadora personal. 
 * [Oracle VirtualBox](https://www.virtualbox.org/)
 * [VMware Workstation](https://www.vmware.com/ar/products/workstation-player/workstation-player-evaluation.html)

#### Comparación

![alt text](img/ContenedoresVSMaquinasVirtuales.JPG)

# Antes de llegar a Docker ...
 * [OpenVZ](https://openvz.org/)
 * [LXC](https://linuxcontainers.org/)

# Que es Docker?
 - Docker nos permite el despliegue de aplicaciones a traves de contenedores de software.

# Docker
## Instalar Docker
 * [Docker Desktop for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows)
 * [Install Docker Engine](https://docs.docker.com/engine/install/)
 * [Install Docker Toolbox on Windows](https://docs.docker.com/toolbox/toolbox_install_windows/)
 
# Arquitectura de Docker

A continuación se muestra el diagrama simple de una arquitectura de Docker.

![alt text](img/DockerArquitectura.JPG)

# Docker Engine
Es la parte central de todo el sistema Docker. Docker Engine es una aplicación que sigue arquitectura cliente-servidor. Está instalado en la máquina host. Hay tres componentes en Docker Engine:

 * **Servidor**: Es el demonio de la ventana acoplable llamado Dockerd. Puede crear y administrar imágenes, contenedores, redes, etc.
 * **API**: Se utiliza para indicar al demonio de Docker qué hacer.
 * **Interfaz de línea de comandos (CLI)**: Es un cliente que se usa para ingresar comandos de docker.

# Interfaz de línea de comandos (CLI)
 * [Use the Docker command line](https://docs.docker.com/engine/reference/commandline/cli/)
 * [26 Comandos de Docker con ejemplos](https://geekflare.com/es/docker-commands/)

# Cliente Docker
Los usuarios de Docker pueden interactuar con Docker a través de un cliente. Cuando se ejecuta cualquier comando de docker, el cliente los envía al demonio dockerd, que los ejecuta. Los comandos de Docker utilizan la API de Docker. El cliente de Docker puede comunicarse con más de un demonio.

# Registros de Docker
Es la ubicación donde se almacenan las imágenes de Docker. Puede ser un registro público o un registro privado. Docker Hub es el lugar predeterminado de las imágenes, el registro público de sus tiendas. También puede crear y ejecutar su propio registro privado.

Cuando ejecuta los comandos docker pull o docker run, la imagen requerida se extrae del registro configurado. Cuando ejecuta el comando docker push, la imagen se almacena en el registro configurado.

 * [Docker Hub](https://hub.docker.com/)

# Objetos Docker
Cuando trabaja con Docker, usa imágenes, contenedores, volúmenes, redes; todos estos son objetos de Docker.

# Imágenes
Las imágenes de Docker son plantillas de solo lectura con instrucciones para crear un contenedor de Docker. La imagen de Docker puede extraerse de un concentrador de Docker y usarse tal como está, o puede agregar instrucciones adicionales a la imagen base y crear una imagen de Docker nueva y modificada. Puede crear sus propias imágenes también utilizando un [dockerfile](https://geekflare.com/es/dockerfile-tutorial/). Cree un dockerfile con todas las instrucciones para crear un contenedor y ejecutarlo; creará su imagen personalizada.

# Contenedores
Después de ejecutar una imagen, crea un contenedor. Todas las aplicaciones y su entorno se ejecutan dentro de este contenedor. Puede usar la API o la CLI de Docker para iniciar, detener y eliminar un contenedor de Docker.

A continuación se muestra un comando de muestra para ejecutar un contenedor docker de ubuntu:
```
docker run -i -t ubuntu /bin/bash
```
# Volúmenes
Los datos persistentes generados por Docker y utilizados por los contenedores de Docker se almacenan en Volumes. Están completamente administrados por Docker a través de Docker CLI o Docker API. Los volúmenes funcionan en contenedores de Windows y Linux. En lugar de conservar los datos en la capa de escritura de un contenedor, siempre es una buena opción utilizar volúmenes para ello. El contenido del volumen existe fuera del ciclo de vida de un contenedor, por lo que usar el volumen no aumenta el tamaño de un contenedor.

Puede usar el indicador -v o –mount para iniciar un contenedor con un volumen. En este comando de muestra, está usando geekvolume volume con el contenedor geekflare.
```
docker run -d --name mynginx  -v myvolume:/app nginx:latest
```